insert into SPECIES(NAME,TRICKS) values ('cat','jump, eat, drink');
insert into ANIMALS(NAME ,TRICKS,SPECIES) values ('lakis','jump','cat');
insert into ANIMALS(NAME ,TRICKS,SPECIES) values ('takis','eat','cat');
insert into ANIMALS(NAME ,TRICKS,SPECIES) values ('hercules','drink','cat');

insert into SPECIES(NAME,TRICKS) values ('fish','');
insert into ANIMALS(name ,tricks,species) values ('john',null,'fish');

insert into SPECIES(NAME,TRICKS) values ('dog','jump,eat,sit,drink');
insert into ANIMALS(name ,tricks,species) values ('aris',null,'dog');

insert into SPECIES(NAME,TRICKS) values ('leon','sleep,drink');
insert into ANIMALS(name ,tricks,species) values ('leonidas',null,'leon');

insert into SPECIES(NAME,TRICKS) values ('horse','jump,run,eat,drink');
insert into ANIMALS(name ,tricks,species) values ('makis',null,'horse');
