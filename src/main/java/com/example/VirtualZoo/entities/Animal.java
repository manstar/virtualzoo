package com.example.VirtualZoo.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="animals")
public class Animal {

        @Id
        @Column(name = "id", nullable = false)
        @GeneratedValue(generator="increment" , strategy = GenerationType.IDENTITY)
        private String id;

        @Column(name = "name")
        private String name;

        @OneToOne(cascade = CascadeType.ALL)
        @JoinColumn(name = "species", referencedColumnName = "name", nullable = false)
        private Species species;
        //@OneToMany(cascade={CascadeType.REMOVE,CascadeType.MERGE, CascadeType.REFRESH}, orphanRemoval = true, mappedBy = "species", targetEntity = Species.class)
        //@Column(name = "species")

        @Column(name = "tricks")
        private String tricks;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Species getSpecies() {
            return species;
        }

        public void setSpecies(Species species) {
            this.species = species;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTricks() {
            return tricks;
        }

        public void setTricks(String tricks) {
            this.tricks = tricks;
        }

}
