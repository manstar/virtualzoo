package com.example.VirtualZoo.entities;

import javax.persistence.*;

@Entity
@Table(name="species")
public class Species {

    @Id
    //@OneToOne(mappedBy = "name")
    @Column(name = "name")
    private String name;

    @Column(name = "tricks")
    private String tricks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTricks() {
        return tricks;
    }

    public void setTricks(String tricks) {
        this.tricks = tricks;
    }
}
