package com.example.VirtualZoo.repos;

import com.example.VirtualZoo.entities.Animal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public interface AnimalRepo extends JpaRepository<Animal, String>{

        @Override
        List<Animal> findAll();

        @Override
        Optional<Animal> findById(String id);
}
