package com.example.VirtualZoo.controllers;


import com.example.VirtualZoo.dtos.AnimalDto;
import com.example.VirtualZoo.dtos.TricksDto;
import com.example.VirtualZoo.entities.Animal;
import com.example.VirtualZoo.services.ZooServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/myZoo")
public class ZooController {

        @Autowired
        ZooServiceImpl zooService;

        @GetMapping("/animals/all")
        public List<AnimalDto> getAnimals() {
            return zooService.findAll();
        }

        @GetMapping("/animals/bySpecies")
        public Map<String,List<AnimalDto>> getAnimalsBySpecies() {
                return zooService.findAnimalsBySpecies();
        }

        @GetMapping("/animals/{animalId}/doTrick")
        public TricksDto doTrick (@PathVariable String animalId) throws Exception {
                return zooService.doTrick(animalId);
        }

        @GetMapping("/animals/{animalId}/learnTrick")
        public String learnTrick (@PathVariable String animalId) throws Exception {
                return zooService.learnTrick(animalId);
        }


}
