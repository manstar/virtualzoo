package com.example.VirtualZoo.dtos;

public final class TricksDto {
    private final String trick;

    public TricksDto(String trick){
        this.trick = trick;
    }

    public String getTrick() {
        return trick;
    }

}
