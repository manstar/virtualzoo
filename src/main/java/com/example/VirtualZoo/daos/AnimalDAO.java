package com.example.VirtualZoo.daos;

import com.example.VirtualZoo.dtos.AnimalDto;
import com.example.VirtualZoo.dtos.TricksDto;
import com.example.VirtualZoo.entities.Animal;
import com.example.VirtualZoo.exceptions.IdNotFoundException;
import com.example.VirtualZoo.repos.AnimalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class AnimalDAO {

    @Autowired
    private AnimalRepo animalRepo;

    private static AnimalDto transformAnimalToAnimalDto(Animal animal){
        final AnimalDto animalDto = new AnimalDto();
        animalDto.setId(animal.getId());
        animalDto.setName(animal.getName());
        animalDto.setSpecies(animal.getSpecies().getName());
        animalDto.setTricks(animal.getTricks());
        return animalDto;
    }

    public List<AnimalDto> findAll() {
        return animalRepo
                       .findAll()
                       .stream()
                       .map(AnimalDAO::transformAnimalToAnimalDto)
                       .collect(Collectors.toList());
    }

    public Map<String,List<AnimalDto>> findAnimalsBySpecies() {
        return  animalRepo
                        .findAll()
                        .stream()
                        .map(AnimalDAO::transformAnimalToAnimalDto)
                        .collect(Collectors.groupingBy(AnimalDto::getSpecies));
    }

    public TricksDto doTrick(String animalId) throws Exception {
         final String tricks = animalRepo
                                       .findById(animalId)
                                       .orElseThrow(IdNotFoundException::new)
                                       .getTricks();
        if(tricks == null || tricks.length() == 0){
            throw new Exception("Animal has not got any trick");
        }
        final String[] tricksTable = tricks.split(",");
        final int tricksTableLength = tricksTable.length;

        if(tricksTableLength == 1){
            return new TricksDto(tricksTable[0]);
        }

        int randomNum = ThreadLocalRandom.current().nextInt(0, tricksTableLength);
        return new TricksDto(tricksTable[randomNum]);
    }

    public String learnTrick(String animalId) throws Exception{
        String trickToLearn = "Animal does not learn a new trick.";

        final Animal animal = animalRepo.findById(animalId).orElseThrow(IdNotFoundException::new);
        final String speciesTricks = animal.getSpecies().getTricks();
        if(speciesTricks != null){
             if(animal.getTricks() != null) {
                 trickToLearn = Stream.of(speciesTricks.split(","))
                         .filter(x -> !animal.getTricks().contains(x))
                         .findFirst()
                         .orElseThrow(() -> new Exception("There is not other trick to learn."));

                 updateAnimalTricks(trickToLearn, animal);
             } else {
                 trickToLearn = speciesTricks.split(",")[0];
                 updateAnimalTricks(trickToLearn, animal);
             }
            trickToLearn = "Animal learns to " + trickToLearn;
        }
        return trickToLearn;
    }

    private void updateAnimalTricks(String trickToLearn, Animal animal) {
        final Animal saveAnimal = new Animal();
        saveAnimal.setId(animal.getId());
        saveAnimal.setSpecies(animal.getSpecies());
        saveAnimal.setName(animal.getName());
        String tricks = animal.getTricks() != null
                ? animal.getTricks() + "," + trickToLearn
                : trickToLearn;
        saveAnimal.setTricks(tricks);
        animalRepo.save(saveAnimal);
    }

}
