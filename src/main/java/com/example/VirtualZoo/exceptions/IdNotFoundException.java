package com.example.VirtualZoo.exceptions;

public class IdNotFoundException extends RuntimeException{
    private String message;

    public IdNotFoundException(String message) {
        super(message);
        this.message = message;
    }
    public IdNotFoundException() {
    }
}
