package com.example.VirtualZoo.services;


import com.example.VirtualZoo.dtos.AnimalDto;
import com.example.VirtualZoo.dtos.TricksDto;

import java.util.List;
import java.util.Map;

public interface ZooService {

    List<AnimalDto> findAll();

    Map<String,List<AnimalDto>> findAnimalsBySpecies();

    TricksDto doTrick(String animalId) throws Exception;

    String learnTrick(String animalId) throws Exception;

}
