package com.example.VirtualZoo.services;


import com.example.VirtualZoo.daos.AnimalDAO;
import com.example.VirtualZoo.dtos.AnimalDto;
import com.example.VirtualZoo.dtos.TricksDto;
import com.example.VirtualZoo.entities.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class ZooServiceImpl implements ZooService {

    @Autowired
    AnimalDAO animalDAO;

    @Override
    public List<AnimalDto> findAll() {
        return animalDAO.findAll();
    }

    @Override
    public Map<String,List<AnimalDto>> findAnimalsBySpecies() {
        return animalDAO.findAnimalsBySpecies();
    }

    @Override
    public TricksDto doTrick(String animalId) throws Exception {
        return animalDAO.doTrick(animalId);
    }

    @Override
    public String learnTrick(String animalId) throws Exception {
        return animalDAO.learnTrick(animalId);
    }


}
